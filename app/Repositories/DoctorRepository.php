<?php

namespace App\Repositories;

use App\Doctor;

class DoctorRepository implements DoctorRepositoryInterface
{
    public function getAllDoctors()
    {
        return Doctor::paginate(10);
    }
    
    public function getApprovedDoctors()
    {
        return Doctor::where('status', true)->paginate(12);
    }

    public function getDoctor($id)
    {
        return Doctor::find($id);
    }

    public function updateDoctorStatus($id)
    {
        $doctor = Doctor::find($id);
        $doctor->status = !$doctor->status;
        $doctor->save();
        return $doctor;
    }
}
