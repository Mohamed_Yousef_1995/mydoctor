<?php

namespace App\Repositories;

use App\Session;

class SessionRepository implements SessionRepositoryInterface
{
    public function createSession($userId, $doctorId)
    {
        return Session::create([
            'user_id' => $userId,
            'doctor_id' => $doctorId,
            'status' => false
        ]);
    }

    public function getAllSessions($id)
    {
        return Session::where('user_id', $id)->orWhere('doctor_id', $id)->paginate(10);
    }

    public function updateSessionStatus($id)
    {
        $session = Session::find($id);
        $session->status = !$session->status;
        $session->save();
        return $session;
    }
}
