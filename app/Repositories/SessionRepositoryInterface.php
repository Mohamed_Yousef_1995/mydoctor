<?php

namespace App\Repositories;

interface SessionRepositoryInterface
{
    public function createSession($userId, $doctorId);
    public function getAllSessions($id);
    public function updateSessionStatus($id);
}