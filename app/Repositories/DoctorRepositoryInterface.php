<?php

namespace App\Repositories;

interface DoctorRepositoryInterface
{
    public function getAllDoctors();
    public function getApprovedDoctors();
    public function getDoctor($id);
    public function updateDoctorStatus($id);
}