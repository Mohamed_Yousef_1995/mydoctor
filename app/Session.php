<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    protected $fillable = [
        'user_id',
        'doctor_id',
        'status'
    ];

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function doctor()
    {
        return $this->hasOne('App\Doctor', 'id', 'doctor_id');
    }
}
