<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DoctorResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->user->name,
            'email' => $this->user->email,
            'specialty' => $this->specialty,
            'description' => $this->description,
            'location' => $this->location,
            'session_fees' => $this->fees,
            'status' => ($this->status)? 'approved' : 'rejected',
            'join_at' => $this->user->created_at
        ];
    }
}
