<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\DoctorRepositoryInterface;
use App\Repositories\SessionRepositoryInterface;
use App\Repositories\UserRepositoryInterface;
use App\Services\NotificationServiceInterface;

class MainController extends Controller
{
    private $doctorRepository;
    private $sessionRepository;
    private $userRepository;
    private $notificationService;

    public function __construct(DoctorRepositoryInterface $doctorRepository, 
        SessionRepositoryInterface $sessionRepository,
        UserRepositoryInterface $userRepository, 
        NotificationServiceInterface $notificationService)
    {
        $this->doctorRepository = $doctorRepository;
        $this->sessionRepository = $sessionRepository;
        $this->userRepository = $userRepository;
        $this->notificationService = $notificationService;
    }

    public function index()
    {
        return view('index', ['doctors' => $this->doctorRepository->getApprovedDoctors()]);
    }
    
    public function reserve($userId, $doctorId)
    {
        $session = $this->sessionRepository->createSession($userId, $doctorId);
        $this->notificationService->sendDoctorSessionNote($session);
        return redirect()->back()->with('success', 'Reservation saved successfully');
    }

    public function sessions()
    {
        $id = (auth()->user()->role != 'doctor')? auth()->user()->id : auth()->user()->doctor->id;

        return view('sessions', [
            'sessions' => $this->sessionRepository->getAllSessions($id)
        ]);
    }

    public function updateSessionStatus($id)
    {
        $session = $this->sessionRepository->updateSessionStatus($id);
        $this->notificationService->sendUserSessionNote($session);
        return redirect()->back()->with('success', 'Session Updated successfully');
    }

    public function notifications()
    {
        return view('notifications', [
            'notifications' => $this->notificationService->getUserNotifications(auth()->user()->id)
        ]);
    }
    
    public function checkNewNotifications()
    {
        return response()->json($this->notificationService->checkUserNewNotifications(auth()->user()->id), 200);
    }
}
