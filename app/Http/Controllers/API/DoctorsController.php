<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\DoctorRepositoryInterface;
use App\Http\Resources\DoctorResource;

class DoctorsController extends Controller
{
    private $doctorRepository;

    public function __construct(DoctorRepositoryInterface $doctorRepository)
    {
        $this->doctorRepository = $doctorRepository;
    }

    public function index()
    {
        return response()->json(DoctorResource::collection(
            $this->doctorRepository->getApprovedDoctors()
        ), 200);
    }
   
    public function show($id)
    {
        $doctor = $this->doctorRepository->getDoctor($id);
        return ($doctor)? response()->json(new DoctorResource($doctor, 200)) : response()->json('Not Found!', 404);
    }
}
