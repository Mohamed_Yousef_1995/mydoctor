<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\DoctorRepositoryInterface;
use App\Services\NotificationServiceInterface;

class BackOfficeController extends Controller
{
    private $doctorRepository;
    private $notificationService;

    public function __construct(DoctorRepositoryInterface $doctorRepository, 
        NotificationServiceInterface $notificationService)
    {
        $this->doctorRepository = $doctorRepository;
        $this->notificationService = $notificationService;
    }

    public function index()
    {
        return view('backoffice', [
            'doctors' => $this->doctorRepository->getAllDoctors()
        ]);
    }

    public function updateDoctorStatus($id)
    {
        $doctor = $this->doctorRepository->updateDoctorStatus($id);
        $this->notificationService->sendDoctorStatusNote($doctor);
        return redirect()->back()->with('success', 'Doctor status Updated successfully');
    }
}
