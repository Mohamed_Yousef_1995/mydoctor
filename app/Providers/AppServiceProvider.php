<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\DoctorRepositoryInterface', 
            'App\Repositories\DoctorRepository'
        );

        $this->app->bind(
            'App\Repositories\SessionRepositoryInterface', 
            'App\Repositories\SessionRepository'
        );
        
        $this->app->bind(
            'App\Repositories\UserRepositoryInterface', 
            'App\Repositories\UserRepository'
        );
        
        $this->app->bind(
            'App\Services\NotificationServiceInterface', 
            'App\Services\NotificationService'
        );
    }


    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
