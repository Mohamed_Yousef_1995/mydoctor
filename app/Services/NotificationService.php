<?php

namespace App\Services;

use Illuminate\Support\Facades\Mail;
use App\Notification;
use App\Mail\NotificationMail;

class NotificationService implements NotificationServiceInterface
{
    private $message;

    private function send($id, $email)
    {
        Notification::create([
            'user_id' => $id,
            'message' => $this->message,
            'seen' => false
        ]);

        try {
            Mail::to($email)->send(new NotificationMail($this->message));
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function sendDoctorStatusNote($doctor)
    {
        $this->message = 'You account has been : ' . (($doctor->status)? 'Approved':'Rejected');
        $this->send($doctor->user->id, $doctor->user->email);
    }
    
    public function sendDoctorSessionNote($session)
    {
        $this->message = 'New session has been reserved <br>';
        $this->message .= ' Please check : <a href = "'.  route('sessions') .'">Click to redirect!</a>';
        $this->send($session->doctor->user->id, $session->doctor->user->email);
    }
    
    public function sendUserSessionNote($session)
    {
        $status = ($session)?'Approved':'Rejected';
        $this->message = "Dr. " . $session->doctor->user->name . " has <b>" . $status . '</b> Your Reservation!';
        $this->send($session->user->id, $session->user->email);
    }

    public function getUserNotifications($id)
    {
        // mark all notifications as seen
        Notification::where('user_id', $id)->update(['seen' => true]);

        return Notification::where('user_id', $id)->latest()->paginate(10);
    }
    
    public function checkUserNewNotifications($id)
    {
        return Notification::where('user_id', $id)->where('seen', false)->count();
    }
}
