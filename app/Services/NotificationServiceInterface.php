<?php

namespace App\Services;

interface NotificationServiceInterface
{
    public function sendDoctorStatusNote($doctor);
    public function sendDoctorSessionNote($session);
    public function sendUserSessionNote($session);
    public function getUserNotifications($id);
    public function checkUserNewNotifications($id);
}