# MyDoctor

Clinic booking system using laravel , bootstrap and jquery 

## Install

* Clone the repo
* run *composer install*
* run *cp .env.example .env*
* set your credintails for both the database and email provider
* run *php artisan key:generate*
* run *php artisan migrate*
* run *php artisan db:seed*

**Now you are ready**
run *php artisan serve*
