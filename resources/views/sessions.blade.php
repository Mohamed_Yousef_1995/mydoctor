@extends('layouts.master')

@section('title', 'MyDoctor - Reservations')

@section('content')
    <div class="row text-center">
        <div class="col-md-12">
            <h1 class="mb-5">List of all reservations</h1>
        </div>

        @if (count($sessions))
            <table class="table table-bordered">
                <thead class="thead-dark">
                    <tr>
                        <th>#</th>
                        <th>{{ (Auth::user()->role === 'doctor')? 'Patient Name': 'Doctor' }}</th>
                        <th>Created At</th>
                        @if (Auth::user()->role === 'doctor')
                            <th>Actions</th>
                        @else
                            <th>Status</th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                    @foreach ($sessions as $session)
                        <tr>
                            <td>
                                {{ $loop->iteration }}
                            </td>
                            <td>
                                {{ (Auth::user()->role === 'doctor')? $session->user->name : $session->doctor->user->name }}
                            </td>
                            <td>
                                {{ $session->created_at }}
                            </td>
                            <td>
                            @if (Auth::user()->role === 'doctor')
                                <a href="{{ route('update_session_status', $session->id) }}" 
                                    class="btn btn-{{ ($session->status)? 'success':'danger' }}"
                                    onclick="return confirm('Are You Sure?');">
                                    @if ($session->status)
                                        <span class="fa fa-check"> Approved</span>
                                    @else
                                        <span class="fa fa-ban"> Rejected</span>
                                    @endif
                                </a>
                            @else
                                <a href="#" class="btn btn-{{ ($session->status)? 'success':'danger' }}">
                                    @if ($session->status)
                                        <span class="fa fa-check"> Approved</span>
                                    @else
                                        <span class="fa fa-ban"> Rejected</span>
                                    @endif
                                </a>
                            @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            <div class="col-md-12 my-5">
                {{ $sessions->links() }}
            </div>
        @else
            <div class="w-100 text-center">
                <h3>Sorry, no results found!</h3>
            </div>
        @endif
    </div>
@endsection