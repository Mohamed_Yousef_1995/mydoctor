@extends('layouts.master')

@section('title', 'MyDoctor')

@section('content')
    <div class="row w-75 mx-auto p-0">
        @forelse ($doctors as $doctor)
            <div class="col-md-4 mb-4">
                <div class="card text-center">
                    <div class="card-body px-1">
                        <h5 class="card-title">{{ 'DR. ' . $doctor->user->name }}</h5>
                        <span class="badge badge-info">{{ $doctor->specialty }}</span>
                        <p class="card-text">{{ $doctor->location }}</p>
                        <p class="card-text">{{ $doctor->description }}</p>

                        <div class="my-3 btn btn-primary w-100">
                            <span class="fa fa-money"></span> {{ "$doctor->fees EGP"}}
                        </div>

                        @if (Auth::check())
                        <a href="{{ route('reserve', [
                                'userId' => Auth::user()->id,
                                'doctorId' => $doctor->id,
                            ]) }}" class="btn btn-success w-100">
                            <span class="fa fa-book"></span> Book Now
                        </a>
                        @else
                        <a href="javascript:;" class="btn btn-success w-100"
                            onclick="alert('Please Register!');">
                            <span class="fa fa-book"></span> Book Now
                        </a>
                        @endif
                    </div>
                </div>
            </div>
        @empty
            <div class="w-100 text-center">
                <h3>Sorry, no results found!</h3>
            </div>
        @endforelse

        @if (count($doctors))
            <div class="col-md-12 my-5">
                {{ $doctors->links() }}
            </div>
        @endif
    </div>
@endsection