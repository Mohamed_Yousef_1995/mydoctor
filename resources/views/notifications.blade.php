@extends('layouts.master')

@section('title', 'MyDoctor - Notifications')

@section('content')
    <div class="row text-center">
        <div class="col-md-12">
            <h1 class="mb-5">Notifications</h1>
        </div>

        @if (count($notifications))
            <div class="col-md-8 offset-md-2">
                <ul class="list-group">
                    @foreach ($notifications as $notification)
                        <li class="list-group-item">
                            <h5>{!! $notification->message !!}</h5>
                            <span class="badge badge-primary">
                                <i class="fa fa-clock-o"></i> {{ $notification->created_at }}
                            </span>
                        </li>
                    @endforeach
                </ul>
            </div>

            <div class="col-md-12 my-5">
                {{ $notifications->links() }}
            </div>
        @else
            <div class="w-100 text-center">
                <h3>Sorry, no results found!</h3>
            </div>
        @endif
    </div>
@endsection