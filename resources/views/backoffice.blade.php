@extends('layouts.master')

@section('title', 'MyDoctor - Backoffice')

@section('content')
    <div class="row text-center">
        <div class="col-md-12">
            <h1 class="mb-5">List of all doctors</h1>
        </div>

        @if (count($doctors))
            <table class="table table-bordered">
                <thead class="thead-dark">
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Specialty</th>
                        <th>Description</th>
                        <th>Location</th>
                        <th>Session Fees (EGP)</th>
                        <th>Joined At</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($doctors as $doctor)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $doctor->user->name }}</td>
                            <td>{{ $doctor->user->email }}</td>
                            <td>{{ $doctor->specialty }}</td>
                            <td>{{ $doctor->description }}</td>
                            <td>{{ $doctor->location }}</td>
                            <td>{{ $doctor->fees }}</td>
                            <td>{{ $doctor->created_at }}</td>
                            <td>
                                <a href="{{ route('update_doctor_status', $doctor->id) }}" 
                                    class="btn btn-{{ ($doctor->status)? 'success':'danger' }}"
                                    onclick="return confirm('Are You Sure?');">
                                    @if ($doctor->status)
                                        <span class="fa fa-check"> Approved</span>
                                    @else
                                        <span class="fa fa-ban"> Rejected</span>
                                    @endif
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            @if (count($doctors))
                <div class="col-md-12 my-5">
                    {{ $doctors->links() }}
                </div>
            @endif
        @else
            <div class="w-100 text-center">
                <h3>Sorry, no results found!</h3>
            </div>
        @endif
    </div>
@endsection