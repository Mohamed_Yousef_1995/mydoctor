<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- [CSS] -->
        <link rel="stylesheet" href="{{ asset('css/bootstrap-4.3.1/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/font-awesome-4.7.0/css/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/style.css') }}">
        <!-- [CSS] -->

        <title>@yield('title')</title>
    </head>
    <body>
        @include('layouts.header')
        
        @if(session()->has('success'))
            <script>alert("{{ session()->get('success') }}");</script>
        @endif

        <div class="search-box bg-info text-center">
            <h3>Find The Best Doctor ...</h3>
        </div>

        <div class="content row">
            <main class="col-md-12 pt-3">
                @yield('content')
            </main>
        </div>

        @include('layouts.footer')

        <!-- [JS] -->
        <script src="{{ asset('js/jquery-3.5.1.min.js') }}"></script>
        <script src="{{ asset('js/popper.js') }}"></script>
        <script src="{{ asset('js/bootstrap-4.3.1/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('js/main.js') }}"></script>
        <!-- [JS] -->
    </body>
</html>
