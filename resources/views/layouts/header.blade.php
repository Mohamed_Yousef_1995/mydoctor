<header>
    <div class="row top-nav">
        <div class="col-md-6">
            <a href=""><span class="fa fa-star"></span> Terms of usage</a> 
            <a href=""><span class="fa fa-check"></span> Privacy and Policy</a> 
            <a href=""><span class="fa fa-dollar"></span> User Center</a>
        </div>
        <div class="col-md-6 text-right">
            <a href=""><span class="fa fa-mobile"></span> 01128389394</a> 
            <a href=""><span class="fa fa-phone"></span> 02-22334455</a> 
            <a href=""><span class="fa fa-envelope"></span> info@my-doctor.com</a>
        </div>
    </div>
    <div class="row bottom-nav">
        <div class="col-md-1 pr-0">
            <div class="logo mx-auto">MD</div>
        </div>
        <div class="col-md-7 pl-0 pt-2">
            <nav class="nav">
                <li class="nav-item">
                    <a class="nav-link active" href="{{ route('home') }}">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">How it works?</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Blog</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Contact</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">About</a>
                </li>
            </nav>
        </div>
        <div class="col-md-4 pt-3 text-center">
            @if (Auth::check())
                @if (Auth::user()->role == 'admin')
                    <a href="{{ route('backoffice') }}" class="btn btn-warning" title="Backoffice">
                        <span class="fa fa-list"></span> Access the Backoffice
                    </a>
                @elseif (Auth::user()->role == 'doctor')
                    <h5 class="d-inline">
                        <span class="btn btn-success">{{ 'Dr. ' . Auth::user()->name }}</span>
                    </h5>
                @else
                    <h5 class="d-inline">
                        <span class="btn btn-success">{{ Auth::user()->name }}</span>
                    </h5>
                @endif

                @if (Auth::user()->role != 'admin')
                    <a href="{{ route('sessions') }}" class="btn btn-warning" title="Reservations">
                        <span class="fa fa-list"></span>
                    </a>
                    <a href="{{ route('notifications') }}" class="btn btn-info" title="Notifications">
                        <span class="fa fa-bell" id="indicator"></span>
                    </a>
                @endif

                <form action="{{ route('logout') }}" method="POST" class="d-inline">
                    @csrf
                    <button class="btn btn-primary d-inline">
                        <span class="fa fa-lg fa-sign-out" title="Log Out"></span>
                    </button>
                </form>
            @else
                <a href="{{ route('login') }}" class="btn btn-info" title="Sign In">
                    <span class="fa fa-user"></span>
                </a>
                <a href="{{ route('register') }}" class="btn btn-success" title="Sign Up">
                    Sign Up
                </a>
                <a href="{{ url('register-doctor') }}">
                    Sign up as a Doctor
                </a>
            @endif
        </div>
    </div>
</header>
