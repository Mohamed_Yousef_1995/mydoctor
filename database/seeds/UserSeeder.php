<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /** Create Admin **/
        App\User::create([
            'name' => 'Admin',
            'email' => 'admin@my-doctor.com',
            'password' => bcrypt('123456'),
            'role' => 'admin',
        ]);
    }
}
