<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'MainController@index')->name('home');

Route::group(['middleware' => ['guest']], function () {
    Route::get('/register-doctor', function (){ return view('auth.register_doctor'); });
    Route::post('/register-doctor', 'Auth\RegisterController@createDoctor')->name('register_doctor');
});

Route::group(['middleware' => ['admin']], function () {
    Route::get('/backoffice', 'BackOfficeController@index')->name('backoffice');
    Route::get('/update-doctor-status/{id}', 'BackOfficeController@updateDoctorStatus')->name('update_doctor_status');
});

Route::group(['middleware' => ['auth']], function () {
    Route::get('/reserve/{userId}/{doctorId}', 'MainController@reserve')->name('reserve');
    Route::get('/sessions', 'MainController@sessions')->name('sessions');
    Route::get('/update-session-status/{id}', 'MainController@updateSessionStatus')->name('update_session_status');
    Route::get('/notifications', 'MainController@notifications')->name('notifications');
    Route::get('/check-new-notifications', 'MainController@checkNewNotifications')->name('check_new_notifications');
});